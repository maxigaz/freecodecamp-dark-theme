# freeCodeCamp Dark Theme

This style has been written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), although it might be compatible with other style managers such as [xStyle](https://github.com/FirefoxBar/xStyle).

The style doesn't cover freeCodeCamp's Forum. However, there's a [separate userstyle](https://gitlab.com/maxigaz/freecodecamp-forum-dark-theme) for that purpose.

## How to install

If you have Stylus installed, click on the banner below and a new window of Stylus should open, asking you to confirm to install the style. For other style managers, you may need to copy and paste the raw css code and update manually.

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/freecodecamp-dark-theme/raw/master/freecodecamp-dark-theme.user.css)

## Screenshots

![A screenshot showing a page of a challenge](Screenshot1.png)

![A screenshot showing a page from the guides](Screenshot2.png)

## Known issues

### Colour changing challenges

In challenges when you have to change the colour of an element that's overriden by the style, the webpage may not recognise what you've set for that element inside the editor. When this happens, temporarily disable the style.
